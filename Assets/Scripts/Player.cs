﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(Animator))]
public class Player : MonoBehaviour {

    private CharacterController m_controller = null;
    private Animator m_animator = null;

    [SerializeField] private float  m_runningSpeed = 80.0f;
    [SerializeField] private float  m_walkingSpeed = 80.0f;
    [SerializeField] private float  m_turningSpeed = 80.0f;
    [SerializeField] private float  m_pushPower = 2.0f;
    [SerializeField] private bool   m_jump = false;
    [SerializeField] private Vector2  m_jumpVelocity = new Vector2(1, 10);
    [SerializeField] private float m_gravityMultiplier = 2;

    public bool m_jumping = false;


    private bool m_isGrounded = true;
    private bool m_wasGrounded = true;

    private float m_groundDistance = 0.1f;
    [SerializeField] private Vector3 m_velocity = Vector3.zero;

    // Use this for initialization
    void Start () {
        m_controller = GetComponent<CharacterController>();
        m_animator = GetComponent<Animator>();
        m_animator.applyRootMotion = false;
        //RaycastHit hit;
        //Physics.Raycast(transform.position, Vector3.down, out hit, Mathf.Infinity, 1 << 8);
        //Debug.Log("Distance to ground: " + hit.distance);
        //Transform[] childrenTransform = GetComponentsInChildren<Transform>();
        //foreach (Transform t in childrenTransform)
        //    t.gameObject.tag = "PlayerParts";
    }

    // Update is called once per frame
    void FixedUpdate () {
        float vertical = Input.GetAxis("Vertical");
        float horizontal = Input.GetAxis("Horizontal");


        if (m_controller.velocity.y <= 0 && m_controller.isGrounded)
        {
            //animator.SetBool("Jump", false);
            m_jumping = false;
        }
        //float cap = Mathf.Abs(horizontal);
        //if (vertical >= 0 && vertical < cap)
        //    vertical = cap;
        //if (vertical < 0 && vertical > -cap)
        //    vertical = -cap;



        //// if you're on the ground, stop accumulating velocity from gravity
        //if (!m_controller.isGrounded)
        //    m_velocity += Physics.gravity * Time.fixedDeltaTime;

        //m_controller.Move(m_velocity * Time.fixedDeltaTime);

        CheckGround();
        JumpSequence();


        if (Input.GetKeyDown(KeyCode.LeftControl) && m_isGrounded)
        {
            CrouchSequence();
        }

        m_animator.SetBool("Jump", m_jumping);


        transform.Rotate(transform.up, horizontal * m_turningSpeed * Time.fixedDeltaTime);

        m_animator.SetFloat("Speed", vertical * m_walkingSpeed * Time.fixedDeltaTime);


        ApplyAnimation();
        m_wasGrounded = m_controller.isGrounded;
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        Rigidbody body = hit.collider.attachedRigidbody;
        if (body == null || body.isKinematic)
            return;

        if (hit.moveDirection.y < -0.3f)
            return;

        Vector3 pushDir = new Vector3(hit.moveDirection.x, 0, hit.moveDirection.z);
        body.velocity = pushDir * m_pushPower;
    }

    private void GroundMovementSequence()
    {

    }

    private void CrouchSequence()
    {


    }

    private void JumpSequence()
    {
        if (!m_controller.isGrounded)
        {
            m_velocity += Physics.gravity * m_gravityMultiplier * Time.fixedDeltaTime;
            m_controller.Move(m_velocity * Time.fixedDeltaTime);
            m_animator.SetFloat("JumpX", m_velocity.y);
            m_animator.SetBool("Falling", true);
        }
        else
        {
            m_velocity.y = 0;
            m_animator.SetBool("Falling", false);
        }
        if (Input.GetKeyDown(KeyCode.Space) && m_controller.isGrounded)
        {
            m_jumping = true;
            m_velocity = m_jumpVelocity.x * transform.forward.normalized + m_jumpVelocity.y * Vector3.up;
            m_animator.SetFloat("JumpX", m_velocity.y);
            m_controller.Move(m_velocity * Time.fixedDeltaTime);            
        }
    }

    private void ApplyAnimation()
    {

    }

    private void CheckGround()
    {
        m_isGrounded = m_controller.isGrounded;
        Debug.Log("isGrounded: " + m_controller.isGrounded);
        if (m_controller.isGrounded/* && m_wasGrounded*/ && m_animator.GetCurrentAnimatorStateInfo(0).IsName("Walking"))
        {
            // Apply root motion means that the animation determines how far the object will move
            // This is compare to treadmill where the animation runs but the object stays in the same place
            m_animator.applyRootMotion = true;
        }
        else
        {
            m_animator.applyRootMotion = false;
        }
        //RaycastHit hit;
        //if (Physics.Raycast(transform.position + m_groundDistance * Vector3.up, Vector3.down, out hit, m_groundDistance))
        //{
        //    m_isGrounded = true;
        //    // Apply root motion means that the animation determines how far the object will move
        //    // This is compare to treadmill where the animation runs but the object stays in the same place
        //    m_animator.applyRootMotion = true;
        //}
        //else
        //{
        //    m_isGrounded = false;
        //    m_animator.applyRootMotion = false;
        //}
        //Debug.Log("isGrounded: " + m_isGrounded);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingPlatform : MonoBehaviour {

    private bool beenStepeed = false;
    private float fallingTimer = 1.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (beenStepeed)
        {
            fallingTimer -= Time.fixedDeltaTime;
        }
        if (fallingTimer <= 0)
        {
            GetComponent<Rigidbody>().isKinematic = false;
        }
	}

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            gameObject.GetComponent<Renderer>().material.SetColor("_Color", Color.red);
            beenStepeed = true;
        }
    }


    //void OnTriggerExit(Collider collider)
    //{
    //    if (collider.gameObject.tag == "Player")
    //    {
    //        collider.gameObject.transform.parent = null;
    //    }
    //}

}

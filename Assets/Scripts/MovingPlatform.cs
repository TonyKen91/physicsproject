﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour {

    private Vector3 direction;

	// Use this for initialization
	void Start () {
        direction = Vector3.right;
    }

    // Update is called once per frame
    void FixedUpdate () {
		if (transform.position.x <= -3.5)
        {
            direction = Vector3.right;
        }
        else if (gameObject.transform.position.x >= 3.5)
        {
            direction = Vector3.left;
        }
        transform.Translate(direction * Time.fixedDeltaTime);

    }

    void OnTriggerEnter(Collider collider)
    {
        Debug.Log("Trigger Occurred");
        Debug.Log(collider.gameObject.tag);
        if (collider.gameObject.tag == "Player")
        {
            Debug.Log("Player Distinguished");
            collider.gameObject.transform.parent = gameObject.transform;
        }
    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            collider.gameObject.transform.parent = null;
        }
    }


}
